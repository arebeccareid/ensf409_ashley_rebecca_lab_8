package lab8_ex3;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

// TODO: Auto-generated Javadoc
/**
 * The Class RebsClientManager.
 */
public class RebsClientManager {

	/** The jdbc connection. */
	public Connection jdbc_connection;
	
	/** The statement. */
	public PreparedStatement statement;
	
	/** The table name. */
	public String databaseName = "ClientDB", tableName = "ClientTable";

	// Students should configure these variables for their own MySQL environment
	// If you have not created your first database in mySQL yet, you can leave the
	// "[DATABASE NAME]" blank to get a connection and create one with the
	/** The password. */
	// createDB() method.
	public String connectionInfo = "jdbc:mysql://localhost:3306/" + databaseName, login = "testingnewuser",
			password = "password";

	/**
	 * Instantiates a new rebs client manager.
	 */
	public RebsClientManager() {
		try {
			// If this throws an error, make sure you have added the mySQL connector JAR to
			// the project
			Class.forName("com.mysql.jdbc.Driver");

			// If this fails make sure your connectionInfo and login/password are correct
			jdbc_connection = DriverManager.getConnection(connectionInfo, login, password);
			System.out.println("Connected to: " + connectionInfo + "\n");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Use the jdbc connection to create a new database in MySQL.
	// (e.g. if you are connected to "jdbc:mysql://localhost:3306", the database
	/**
	 * Creates the DB.
	 */
	// will be created at "jdbc:mysql://localhost:3306/InventoryDB")
	public void createDB() {
		try {
			statement = jdbc_connection.prepareStatement("CREATE DATABASE " + databaseName);
			statement.executeUpdate();
			System.out.println("Created Database " + databaseName);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 *  Create a data table inside of the database to hold tools.
	 */
	public void createTable() {
		String sql = "CREATE TABLE " + tableName + "(" + "ID INT(4) NOT NULL, " + "FIRSTNAME VARCHAR(20) NOT NULL, "
				+ "LASTNAME VARCHAR(20) NOT NULL, " + "ADDRESS VARCHAR(50) NOT NULL, "
				+ "POSTALCODE VARCHAR(7) NOT NULL, " + "PHONENUMBER BIGINT(12) NOT NULL, "
				+ "CLIENTTYPE VARCHAR(1) NOT NULL, " + "PRIMARY KEY ( id ))";
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.executeUpdate();
			System.out.println("Created Table " + tableName);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Removes the data table from the database (and all the data held within it!).
	 */
	public void removeTable() {
		String sql = "DROP TABLE " + tableName;
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.executeUpdate();
			System.out.println("Removed Table " + tableName);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adds the item.
	 *
	 * @param client the client
	 */
	// Add a tool to the database table
	public void addItem(Client client) {
		String sql = "INSERT INTO " + tableName + " VALUES ( ?, ?, ?, ?, ?, ?, ?);";

		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.setInt(1, client.getId());
			statement.setString(2, client.getFirstname());
			statement.setString(3, client.getLastname());
			statement.setString(4, client.getAddress());
			statement.setString(5, client.getPostalCode());
			statement.setLong(6, client.getPhoneNumber());
			statement.setString(7, String.valueOf(client.getClientType()));
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method should search the database table for a client matching the ID
	 * parameter and return that Client. It should return null if no Client matching
	 * that ID are found.
	 *
	 * @param clientID the client ID
	 * @return the array list
	 */
	public ArrayList<Client> searchforID(int clientID) {
		// String sql = "SELECT * FROM " + tableName + " WHERE ID=" + toolID;
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ?";
		ResultSet myClient;
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.setInt(1, clientID);
			myClient = statement.executeQuery();
			ArrayList <Client> clientMatch  = new ArrayList<Client>();
			
			while (myClient.next()) {
				// public Client(int id, String firstname, String lastname, String address,
				// String postalCode, int phoneNumber,char clientType)
				clientMatch.add( new Client(myClient.getInt("ID"), myClient.getString("FIRSTNAME"),
						myClient.getString("LASTNAME"), myClient.getString("ADDRESS"), myClient.getString("POSTALCODE"),
						myClient.getLong("PHONENUMBER"), (myClient.getString("CLIENTTYPE")).charAt(0)));
			}
			
			return clientMatch;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	/**
	 * Searchfor name.
	 *
	 * @param lastName the last name
	 * @return the array list
	 */
	public ArrayList<Client> searchforName(String lastName) {
		String sql = "SELECT * FROM " + tableName + " WHERE LASTNAME = ?";
		ResultSet myClient;
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.setString(1, lastName);
			myClient = statement.executeQuery();
			ArrayList <Client> clientMatch = new ArrayList<Client>();
			while (myClient.next()) {
				// public Client(int id, String firstname, String lastname, String address,
				// String postalCode, int phoneNumber,char clientType)
				clientMatch.add( new Client(myClient.getInt("ID"), myClient.getString("FIRSTNAME"),
						myClient.getString("LASTNAME"), myClient.getString("ADDRESS"), myClient.getString("POSTALCODE"),
						myClient.getLong("PHONENUMBER"), (myClient.getString("CLIENTTYPE")).charAt(0)));
			}
			
			return clientMatch;
			
			

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	/**
	 * Searchfor type.
	 *
	 * @param clientType the client type
	 * @return the array list
	 */
	public ArrayList<Client> searchforType(String clientType) {
		String sql = "SELECT * FROM " + tableName + " WHERE CLIENTTYPE = ?";
		ResultSet myClient;
		
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.setString(1, clientType);
			myClient = statement.executeQuery();
			ArrayList<Client> clientMatch  = new ArrayList<Client>();
			
			while (myClient.next()) {
				// public Client(int id, String firstname, String lastname, String address,
				// String postalCode, int phoneNumber,char clientType)
				
				clientMatch.add( new Client(myClient.getInt("ID"), myClient.getString("FIRSTNAME"),
						myClient.getString("LASTNAME"), myClient.getString("ADDRESS"), myClient.getString("POSTALCODE"),
						myClient.getLong("PHONENUMBER"), (myClient.getString("CLIENTTYPE")).charAt(0)));
				
				
			}
			return clientMatch;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	/**
	 * Delete.
	 *
	 * @param id the id
	 */
	public void delete(int id) {
		String sql = "DELETE FROM " + tableName + " WHERE ID = ?";
		
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeUpdate();
			
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Prints all the items in the database to console.
	 */
	public void printTable() {
		try {
			String sql = "SELECT * FROM " + tableName;
			statement = jdbc_connection.prepareStatement(sql);
			ResultSet myClient = statement.executeQuery();
			System.out.println("Clients:");
			while (myClient.next()) {
				System.out.println(myClient.getInt("ID") + " " + myClient.getString("FIRSTNAME") + " "
						+ myClient.getString("LASTNAME") + " " + myClient.getString("ADDRESS") + " "
						+ myClient.getString("POSTALCODE") + " " + myClient.getLong("PHONENUMBER") + " "
						+ (myClient.getString("CLIENTTYPE")).charAt(0));
			}
			myClient.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//
//	}

}
