package lab8_ex3;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

// TODO: Auto-generated Javadoc
/**
 * The Class CenterRightPanel.
 */
public class CenterRightPanel{
	
	/** The panel. */
	private JPanel panel;
	
	/** The c. */
	private GridBagConstraints c;
	
	/** The client ID. */
	private Field clientID;
	
	/** The first name. */
	private Field firstName;
	
	/** The last name. */
	private Field lastName;
	
	/** The address. */
	private Field address;
	
	/** The postal code. */
	private Field postalCode;
	
	/** The phone number. */
	private Field phoneNumber;
	
	/** The client type. */
	private ClientTypeButtons clientType;
	
	/** The id. */
	private static int id;
	
	/** The row. */
	private int row;
	
	/** The buttons. */
	private LowerRightButtons buttons;
	

	
	
	/**
	 * Instantiates a new center right panel.
	 */
	public CenterRightPanel() {
		panel = new JPanel(new GridBagLayout());
		panel.setBorder(BorderFactory.createLineBorder(Color.black));
		c = new GridBagConstraints();
		c.weightx = 0.5;
		c.weighty = 0.5;
		row = 0;
		
		title();
		textFields();
		
		clientTypeSelector();
		
		buttons();
		
	}
	
	/**
	 * Title.
	 */
	private void title() {
		c.gridx = 0;
		c.gridy = row++;
		panel.add(new JLabel("Client Information"), c);
	}
	
	/**
	 * Text fields.
	 */
	private void textFields() {
		clientID = new Field(panel, row++, "Client ID: ");
		clientID.getText().setEditable(false);
		String sID = String.valueOf(id++);
		clientID.getText().setText(sID);
		
		firstName = new Field(panel, row++,  "First Name: ");
		lastName = new Field(panel, row++, "Last Name: ");
		address = new Field(panel, row++, "Address: ");
		postalCode = new Field(panel, row++, "Postal Code: ");
		phoneNumber = new Field(panel, row++, "Phone Number: ");
		
	}
	
	/**
	 * Client type selector.
	 */
	private void clientTypeSelector() {
		
		
		c.gridx = 0;
		c.gridy = row++;
		panel.add(new JLabel("Client Type"), c);
		
		clientType = new ClientTypeButtons();
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = row++;
		panel.add(clientType.getResidentialButton(), c);
		c.gridy = row++;
		panel.add(clientType.getCommersialButton(), c);
		c.gridwidth = 1;
		
	}
	
	/**
	 * Buttons.
	 */
	private void buttons() {
		buttons = new LowerRightButtons();
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridy = row++;
		c.gridx = 0;
	
		panel.add(buttons.getSave(), c);
		
		c.gridx = 1;
		panel.add(buttons.getDelete(), c);
		
		c.gridx = 2;
		panel.add(buttons.getClear(), c);
	}
	
	/**
	 * Contents.
	 *
	 * @return the string
	 */
	public String contents() {
		String id = clientID.getText().getText();
		String fn = firstName.getText().getText();
		String ln = lastName.getText().getText();
		String a = address.getText().getText();
		String pc = postalCode.getText().getText();
		String pn = phoneNumber.getText().getText();
		
		return id + fn + ln + a + pc + pn;
	}
	
	/**
	 * Sets the all field.
	 *
	 * @param client the new all field
	 */
	public void setAllField(Client client) {
		clientID.display(String.valueOf(client.getId()));
		firstName.display(client.getFirstname());
		lastName.display(client.getLastname());
		address.display(client.getAddress());
		postalCode.display(client.getPostalCode());
		
		String number = restorePhoneNumber(client.getPhoneNumber());
		phoneNumber.display(number);
		
		clientType.setSelected(client.getClientType());
		
	}
	
	/**
	 * Restore phone number.
	 *
	 * @param rawNumber the raw number
	 * @return the string
	 */
	public String restorePhoneNumber(Long rawNumber) {
		String number = rawNumber.toString();
		String restored = number.substring(0, 3) + "-" + number.substring(3, 6) + "-" + number.substring(6, 10);
		return restored;
	}

	/**
	 * Gets the panel.
	 *
	 * @return the panel
	 */
	public JPanel getPanel() {
		return panel;
	}

	/**
	 * Sets the panel.
	 *
	 * @param panel the new panel
	 */
	public void setPanel(JPanel panel) {
		this.panel = panel;
	}


	/**
	 * Gets the client ID.
	 *
	 * @return the client ID
	 */
	public Field getClientID() {
		return clientID;
	}


	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public Field getFirstName() {
		return firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public Field getLastName() {
		return lastName;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public Field getAddress() {
		return address;
	}



	/**
	 * Gets the postal code.
	 *
	 * @return the postal code
	 */
	public Field getPostalCode() {
		return postalCode;
	}

	/**
	 * Gets the phone number.
	 *
	 * @return the phone number
	 */
	public Field getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Gets the client type.
	 *
	 * @return the client type
	 */
	public ClientTypeButtons getClientType() {
		return clientType;
	}

	/**
	 * Gets the buttons.
	 *
	 * @return the buttons
	 */
	public LowerRightButtons getButtons() {
		return buttons;
	}

	/**
	 * New ID.
	 */
	public void newID() {
		String sID = String.valueOf(id++);
		clientID.getText().setText(sID);
	}
}
	
