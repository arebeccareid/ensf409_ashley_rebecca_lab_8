package lab8_ex3;

import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

// TODO: Auto-generated Javadoc
/**
 * The Class ClientTypeButtons.
 */
public class ClientTypeButtons{
	
	/** The residential button. */
	private JRadioButton residentialButton;
	
	/** The commersial button. */
	private JRadioButton commersialButton;
	
	/** The group. */
	private ButtonGroup group;
	
	/**
	 * Instantiates a new client type buttons.
	 */
	public ClientTypeButtons() {

	
		
		residentialButton = new JRadioButton("Residential");
		residentialButton.setName("R");
		
		commersialButton = new JRadioButton("Commersial");
		commersialButton.setName("C");
		setup();
	}
	
	/**
	 * Setup.
	 */
	public void setup() {

		
		residentialButton.setSelected(true);
		
		
		group = new ButtonGroup();

		group.add(residentialButton);
		group.add(commersialButton);
		
		
	}

	/**
	 * Gets the selected button.
	 *
	 * @return the selected button
	 */
	public String getSelectedButton()
	{  
	    for (Enumeration<AbstractButton> buttons = group.getElements(); buttons.hasMoreElements();) {
	            AbstractButton button = buttons.nextElement();
	            if (button.isSelected()) {
	                return button.getName();
	            }
	        }
	return null;
	}
	
	
	/**
	 * Sets which button is selected.
	 *
	 * @param s the new selected
	 */
	public void setSelected(char s) {
		
		if(s == 'R' || s == 'r') {
			residentialButton.setSelected(true);
		}
		else if(s == 'C' || s == 'c') {
			commersialButton.setSelected(true);
		}
		
	}
	
	/**
	 * Gets the residential button.
	 *
	 * @return the residential button
	 */
	public JRadioButton getResidentialButton() {
		return residentialButton;
	}

	/**
	 * Gets the commersial button.
	 *
	 * @return the commersial button
	 */
	public JRadioButton getCommersialButton() {
		return commersialButton;
	}
	
	
}