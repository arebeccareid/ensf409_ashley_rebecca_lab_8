package lab8_ex3;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;

// TODO: Auto-generated Javadoc
/**
 * The Class ClientView.
 */
public class ClientView {
	
	/** The frame. */
	private JFrame frame;
	
	/** The main panel. */
	private MainPanel mainPanel;

	/**
	 * Instantiates a new client view.
	 */
	public ClientView() {
		frame = new JFrame();
		mainPanel = new MainPanel();
		
	}
	
	/**
	 * Setup.
	 */
	public void setup() {
		mainPanel.setup();
		frame.setSize(500, 500);
		frame.add(mainPanel.getPanel());
		
		frame.setVisible(true);
	}

	
	
	/**
	 * Gets the frame.
	 *
	 * @return the frame
	 */
	public JFrame getFrame() {
		return frame;
	}

	

	/**
	 * Gets the main panel.
	 *
	 * @return the main panel
	 */
	public MainPanel getMainPanel() {
		return mainPanel;
	}
	
}
