package lab8_ex2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

// TODO: Auto-generated Javadoc
/**
 * The Class ClientControl.
 */
public class ClientControl {

	/** The view. */
	private ClientView view;

	/** The model. */
	private RebsClientManager model;

	/**
	 * Instantiates a new client control.
	 */
	public ClientControl() {
		view = new ClientView();

		model = new RebsClientManager();
	}

	/**
	 * Runs the control.
	 */
	public void run() {
		view.setup();

		closeFrame();
		addRightPanelActionListeners();
		addLeftActionListeners();

		model.createTable();

	}

	/**
	 * Brings up the option to close frame when window closed
	 */
	private void closeFrame() {

		view.getFrame().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		view.getFrame().addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				JFrame frame = (JFrame) e.getSource();

				int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to exit the application?",
						"Exit Application", JOptionPane.YES_NO_OPTION);

				if (result == JOptionPane.YES_OPTION) {
					model.removeTable();
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				}
			}
		});
	}

	/**
	 * Adds the right panel action listeners.
	 */
	private void addRightPanelActionListeners() {
		// should id increment?
		// Add action listeners
		view.getMainPanel().getCenter().getRight().getButtons().addSaveActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//

				int id = Integer.parseInt(view.getMainPanel().getCenter().getRight().getClientID().getText().getText());
				String firstName = view.getMainPanel().getCenter().getRight().getFirstName().getText().getText();
				String lastName = view.getMainPanel().getCenter().getRight().getLastName().getText().getText();
				String address = view.getMainPanel().getCenter().getRight().getAddress().getText().getText();
				String postalCode = view.getMainPanel().getCenter().getRight().getPostalCode().getText().getText();
				int phoneNumber = Integer
						.parseInt(view.getMainPanel().getCenter().getRight().getPhoneNumber().getText().getText());
				char clientType = view.getMainPanel().getCenter().getRight().getClientType().getSelectedButton()
						.charAt(0);

				Client newClient = new Client(id, firstName, lastName, address, postalCode, phoneNumber, clientType);

				model.addItem(newClient);
				//

			}

		});

		view.getMainPanel().getCenter().getRight().getButtons().addClearActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				// System.out.println("cleared");

//				view.getMainPanel().getCenter().getRight().getClientID().getText().setText("");
				view.getMainPanel().getCenter().getRight().newID();
				view.getMainPanel().getCenter().getRight().getFirstName().getText().setText("");
				view.getMainPanel().getCenter().getRight().getLastName().getText().setText("");
				view.getMainPanel().getCenter().getRight().getAddress().getText().setText("");
				view.getMainPanel().getCenter().getRight().getPostalCode().getText().setText("");
				view.getMainPanel().getCenter().getRight().getPhoneNumber().getText().setText("");
			}

		});

		view.getMainPanel().getCenter().getRight().getButtons().addDeleteActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				// System.out.println("deleted");

				int id = Integer.parseInt(view.getMainPanel().getCenter().getRight().getClientID().getText().getText());
				// String firstName =
				// view.getMainPanel().getCenter().getRight().getFirstName().getText().getText();
				// String lastName =
				// view.getMainPanel().getCenter().getRight().getLastName().getText().getText();
				// String address =
				// view.getMainPanel().getCenter().getRight().getAddress().getText().getText();
				// String postalCode =
				// view.getMainPanel().getCenter().getRight().getPostalCode().getText().getText();
				// int phoneNumber =
				// Integer.parseInt(view.getMainPanel().getCenter().getRight().getPhoneNumber().getText().getText());
				// char clientType =
				// view.getMainPanel().getCenter().getRight().getClientType().getSelectedButton().charAt(0);
				//
				// Client deletedClient = new Client(id, firstName, lastName, address,
				// postalCode, phoneNumber, clientType);

				model.delete(id);

			}

		});
	}

	/**
	 * Adds the left action listeners.
	 */
	private void addLeftActionListeners() {

		view.getMainPanel().getCenter().getLeft().getTopPanel().addClearActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				System.out.println("clear search");
				view.getMainPanel().getCenter().getLeft().getLowerPanel().getResults().clear();
			}

		});

		view.getMainPanel().getCenter().getLeft().getTopPanel().addSearchActionListener(new ActionListener() {

			@Override
			/**
			 * call to database to get search results
			 */
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				// System.out.println("search");

				// String searchResults;
				// call to database to get search results
				String searchType = view.getMainPanel().getCenter().getLeft().getTopPanel().getSearchType()
						.getSelectedButton();

				String searchTerm = view.getMainPanel().getCenter().getLeft().getTopPanel().getSearchTerm().getText()
						.getText();
				if (searchTerm == null || searchTerm.equals("")) {
					JOptionPane.showMessageDialog(null, "Please enter a search term", "Error",
							JOptionPane.PLAIN_MESSAGE);
					return;
				}

				ArrayList<Client> searchResults;

				if (searchType.equals("ID")) {
					int clientID = Integer.parseInt(searchTerm);
					searchResults = model.searchforID(clientID);

				} else if (searchType.equals("NAME")) {
					searchResults = model.searchforName(searchTerm);
				} else if (searchType.equals("TYPE")) {

					String shortTerm = searchTerm.substring(0, 1);
					searchResults = model.searchforType(shortTerm);
				} else {
					JOptionPane.showMessageDialog(null, "Error specifying search type", " Message",
							JOptionPane.PLAIN_MESSAGE);
					return;
				}

				if (searchResults.isEmpty()) {
					JOptionPane.showMessageDialog(null, "No results found", " Message", JOptionPane.PLAIN_MESSAGE);
				} else {
					for (Client c : searchResults)
						view.getMainPanel().getCenter().getLeft().getLowerPanel().getResults().addClient(c);
				}

			}

		});

		view.getMainPanel().getCenter().getLeft().getLowerPanel().getResults()
				.addListSelectionListener(new ListSelectionListener() {

					@Override
					public void valueChanged(ListSelectionEvent e) {
						int index = view.getMainPanel().getCenter().getLeft().getLowerPanel().getResults()
								.getListOfResults().getSelectedIndex();
						if (index != -1) {
							Client selected = (Client) view.getMainPanel().getCenter().getLeft().getLowerPanel()
									.getResults().getListModel().getElementAt(index);
							// System.out.println(selected);
							view.getMainPanel().getCenter().getRight().setAllField(selected);
						}
					}

				});
	}

}