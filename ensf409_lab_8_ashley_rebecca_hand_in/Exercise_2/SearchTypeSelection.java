package lab8_ex2;

import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

// TODO: Auto-generated Javadoc
/**
 * The Class SearchTypeSelection.
 */
public class SearchTypeSelection {

	/** The client ID. */
	private JRadioButton clientID;

	/** The last name. */
	private JRadioButton lastName;

	/** The client type. */
	private JRadioButton clientType;

	/** The group. */
	private ButtonGroup group;

	/**
	 * Instantiates a new search type selection.
	 */
	public SearchTypeSelection() {
		clientID = new JRadioButton("Client ID");
		clientID.setName("ID");

		lastName = new JRadioButton("Last Name");
		lastName.setName("NAME");

		clientType = new JRadioButton("Client Type");
		clientType.setName("TYPE");

		group = new ButtonGroup();
		group.add(clientID);
		group.add(lastName);
		group.add(clientType);

		clientID.setSelected(true);

	}

	/**
	 * Gets the selected button.
	 *
	 * @return the selected button
	 */
	public String getSelectedButton() {
		for (Enumeration<AbstractButton> buttons = group.getElements(); buttons.hasMoreElements();) {
			AbstractButton button = buttons.nextElement();
			if (button.isSelected()) {
				return button.getName();
			}
		}
		return null;
	}

	/**
	 * Gets the client ID.
	 *
	 * @return the client ID
	 */
	public JRadioButton getClientID() {
		return clientID;
	}

	/**
	 * Sets the client ID.
	 *
	 * @param clientID
	 *            the new client ID
	 */
	public void setClientID(JRadioButton clientID) {
		this.clientID = clientID;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public JRadioButton getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName
	 *            the new last name
	 */
	public void setLastName(JRadioButton lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the client type.
	 *
	 * @return the client type
	 */
	public JRadioButton getClientType() {
		return clientType;
	}

	/**
	 * Sets the client type.
	 *
	 * @param clientType
	 *            the new client type
	 */
	public void setClientType(JRadioButton clientType) {
		this.clientType = clientType;
	}

}