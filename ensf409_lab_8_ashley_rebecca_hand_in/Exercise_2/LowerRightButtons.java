package lab8_ex2;

import java.awt.event.ActionListener;

import javax.swing.JButton;

// TODO: Auto-generated Javadoc
/**
 * The Class LowerRightButtons.
 */
public class LowerRightButtons {

	/** The save. */
	private JButton save;

	/** The clear. */
	private JButton clear;

	/** The delete. */
	private JButton delete;

	/**
	 * Instantiates a new lower right buttons.
	 */
	public LowerRightButtons() {
		save = new JButton("Save");
		clear = new JButton("Clear");
		delete = new JButton("Delete");
	}

	/**
	 * Adds the save action listener.
	 *
	 * @param actionListener
	 *            the action listener
	 */
	public void addSaveActionListener(ActionListener actionListener) {
		save.addActionListener(actionListener);
	}

	/**
	 * Adds the clear action listener.
	 *
	 * @param al
	 *            the al
	 */
	public void addClearActionListener(ActionListener al) {
		clear.addActionListener(al);
	}

	/**
	 * Adds the delete action listener.
	 *
	 * @param al
	 *            the al
	 */
	public void addDeleteActionListener(ActionListener al) {
		delete.addActionListener(al);
	}

	/**
	 * Gets the save.
	 *
	 * @return the save
	 */
	public JButton getSave() {
		return save;
	}

	/**
	 * Sets the save.
	 *
	 * @param save
	 *            the new save
	 */
	public void setSave(JButton save) {
		this.save = save;
	}

	/**
	 * Gets the clear.
	 *
	 * @return the clear
	 */
	public JButton getClear() {
		return clear;
	}

	/**
	 * Sets the clear.
	 *
	 * @param clear
	 *            the new clear
	 */
	public void setClear(JButton clear) {
		this.clear = clear;
	}

	/**
	 * Gets the delete.
	 *
	 * @return the delete
	 */
	public JButton getDelete() {
		return delete;
	}

	/**
	 * Sets the delete.
	 *
	 * @param delete
	 *            the new delete
	 */
	public void setDelete(JButton delete) {
		this.delete = delete;
	}

}