package lab8_ex2;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

// TODO: Auto-generated Javadoc
/**
 * The Class CenterRightPanel.
 */
public class CenterRightPanel {

	/** The panel. */
	private JPanel panel;

	/** The c. */
	private GridBagConstraints c;

	/** The client ID. */
	private Field clientID;

	/** The first name. */
	private Field firstName;

	/** The last name. */
	private Field lastName;

	/** The address. */
	private Field address;

	/** The postal code. */
	private Field postalCode;

	/** The phone number. */
	private Field phoneNumber;

	/** The client type. */
	private ClientTypeButtons clientType;

	/** The id. */
	private static int id;

	/** The row. */
	private int row;

	/** The buttons. */
	private LowerRightButtons buttons;

	/**
	 * Instantiates a new center right panel.
	 */
	public CenterRightPanel() {
		panel = new JPanel(new GridBagLayout());
		panel.setBorder(BorderFactory.createLineBorder(Color.black));
		c = new GridBagConstraints();
		c.weightx = 0.5;
		c.weighty = 0.5;
		row = 0;

		title();
		textFields();

		clientTypeSelector();

		buttons();

	}

	/**
	 * Creates a new Title, and adds it to the panel. increments row to add more
	 * titles.
	 */
	private void title() {
		c.gridx = 0;
		c.gridy = row++;
		panel.add(new JLabel("Client Information"), c);
	}

	/**
	 * Adds the text fields to be displayed to the panel.
	 */
	private void textFields() {
		clientID = new Field(panel, row++, "Client ID: ");
		clientID.getText().setEditable(false);
		String sID = String.valueOf(id++);
		clientID.getText().setText(sID);

		firstName = new Field(panel, row++, "First Name: ");
		lastName = new Field(panel, row++, "Last Name: ");
		address = new Field(panel, row++, "Address: ");
		postalCode = new Field(panel, row++, "Postal Code: ");
		phoneNumber = new Field(panel, row++, "Phone Number: ");

	}

	/**
	 * Client type selector.
	 */
	private void clientTypeSelector() {

		c.gridx = 0;
		c.gridy = row++;
		panel.add(new JLabel("Client Type"), c);

		clientType = new ClientTypeButtons();
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = row++;
		panel.add(clientType.getResidentialButton(), c);
		c.gridy = row++;
		panel.add(clientType.getCommersialButton(), c);
		c.gridwidth = 1;

	}

	/**
	 * Buttons.
	 */
	private void buttons() {
		buttons = new LowerRightButtons();

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridy = row++;
		c.gridx = 0;

		panel.add(buttons.getSave(), c);

		c.gridx = 1;
		panel.add(buttons.getDelete(), c);

		c.gridx = 2;
		panel.add(buttons.getClear(), c);
	}

	/**
	 * returns Contents of the fields.
	 *
	 * @return the string
	 */
	public String contents() {
		String id = clientID.getText().getText();
		String fn = firstName.getText().getText();
		String ln = lastName.getText().getText();
		String a = address.getText().getText();
		String pc = postalCode.getText().getText();
		String pn = phoneNumber.getText().getText();

		return id + fn + ln + a + pc + pn;
	}

	/**
	 * Sets all fields to the values of the inputted client
	 *
	 * @param client
	 *            the new field values
	 */
	public void setAllField(Client client) {
		clientID.display(String.valueOf(client.getId()));
		firstName.display(client.getFirstname());
		lastName.display(client.getLastname());
		address.display(client.getAddress());
		postalCode.display(client.getPostalCode());
		phoneNumber.display(String.valueOf(client.getPhoneNumber()));

		clientType.setSelected(client.getClientType());

	}

	/**
	 * Gets the panel.
	 *
	 * @return the panel
	 */
	public JPanel getPanel() {
		return panel;
	}

	/**
	 * Sets the panel.
	 *
	 * @param panel
	 *            the new panel
	 */
	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	/**
	 * Gets the c.
	 *
	 * @return the c
	 */
	public GridBagConstraints getC() {
		return c;
	}

	/**
	 * Sets the c.
	 *
	 * @param c
	 *            the new c
	 */
	public void setC(GridBagConstraints c) {
		this.c = c;
	}

	/**
	 * Gets the client ID.
	 *
	 * @return the client ID
	 */
	public Field getClientID() {
		return clientID;
	}

	/**
	 * Sets the client ID.
	 *
	 * @param clientID
	 *            the new client ID
	 */
	public void setClientID(Field clientID) {
		this.clientID = clientID;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public Field getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName
	 *            the new first name
	 */
	public void setFirstName(Field firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public Field getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName
	 *            the new last name
	 */
	public void setLastName(Field lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public Field getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address
	 *            the new address
	 */
	public void setAddress(Field address) {
		this.address = address;
	}

	/**
	 * Gets the postal code.
	 *
	 * @return the postal code
	 */
	public Field getPostalCode() {
		return postalCode;
	}

	/**
	 * Sets the postal code.
	 *
	 * @param postalCode
	 *            the new postal code
	 */
	public void setPostalCode(Field postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * Gets the phone number.
	 *
	 * @return the phone number
	 */
	public Field getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Sets the phone number.
	 *
	 * @param phoneNumber
	 *            the new phone number
	 */
	public void setPhoneNumber(Field phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Gets the client type.
	 *
	 * @return the client type
	 */
	public ClientTypeButtons getClientType() {
		return clientType;
	}

	/**
	 * Sets the client type.
	 *
	 * @param clientType
	 *            the new client type
	 */
	public void setClientType(ClientTypeButtons clientType) {
		this.clientType = clientType;
	}

	/**
	 * Gets the row.
	 *
	 * @return the row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Sets the row.
	 *
	 * @param row
	 *            the new row
	 */
	public void setRow(int row) {
		this.row = row;
	}

	/**
	 * Gets the buttons.
	 *
	 * @return the buttons
	 */
	public LowerRightButtons getButtons() {
		return buttons;
	}

	/**
	 * Sets the buttons.
	 *
	 * @param buttons
	 *            the new buttons
	 */
	public void setButtons(LowerRightButtons buttons) {
		this.buttons = buttons;
	}

	/**
	 * New ID.
	 */
	public void newID() {
		String sID = String.valueOf(id++);
		clientID.getText().setText(sID);
	}
}