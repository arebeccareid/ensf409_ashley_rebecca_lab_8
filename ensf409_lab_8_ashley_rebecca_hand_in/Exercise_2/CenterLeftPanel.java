package lab8_ex2;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

// TODO: Auto-generated Javadoc
/**
 * The Class CenterLeftPanel.
 */
public class CenterLeftPanel {

	/** The panel. */
	private JPanel panel;

	/** The top panel. */
	private CenterLeftTopPanel topPanel;

	/** The lower panel. */
	private CenterLeftLowerPanel lowerPanel;

	/**
	 * Instantiates a new center left panel.
	 */
	public CenterLeftPanel() {
		panel = new JPanel(new GridLayout(2, 1));

		topPanel = new CenterLeftTopPanel();
		lowerPanel = new CenterLeftLowerPanel();
	}

	/**
	 * Sets up the panel.
	 */
	public void setup() {

		topPanel.setup();
		panel.add(topPanel.getPanel());

		lowerPanel.setup();
		panel.add(lowerPanel.getPanel());

	}

	/**
	 * Gets the panel.
	 *
	 * @return the panel
	 */
	public JPanel getPanel() {
		return panel;
	}

	/**
	 * Sets the panel.
	 *
	 * @param panel
	 *            the new panel
	 */
	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	/**
	 * Gets the top panel.
	 *
	 * @return the top panel
	 */
	public CenterLeftTopPanel getTopPanel() {
		return topPanel;
	}

	/**
	 * Sets the top panel.
	 *
	 * @param topPanel
	 *            the new top panel
	 */
	public void setTopPanel(CenterLeftTopPanel topPanel) {
		this.topPanel = topPanel;
	}

	/**
	 * Gets the lower panel.
	 *
	 * @return the lower panel
	 */
	public CenterLeftLowerPanel getLowerPanel() {
		return lowerPanel;
	}

	/**
	 * Sets the lower panel.
	 *
	 * @param lowerPanel
	 *            the new lower panel
	 */
	public void setLowerPanel(CenterLeftLowerPanel lowerPanel) {
		this.lowerPanel = lowerPanel;
	}

}