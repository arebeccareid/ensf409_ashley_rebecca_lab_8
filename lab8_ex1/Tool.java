package lab8_ex1;

// TODO: Auto-generated Javadoc
/**
 * The Class Tool.
 */
public class Tool {

	/** The id. */
	private int id;

	/** The name. */
	private String name;

	/** The quantity. */
	private int quantity;

	/** The price. */
	private double price;

	/** The supplier ID. */
	private int supplierID;

	/**
	 * Instantiates a new tool.
	 *
	 * @param id
	 *            the id
	 * @param name
	 *            the name
	 * @param quantity
	 *            the quantity
	 * @param price
	 *            the price
	 * @param supplierID
	 *            the supplier ID
	 */
	public Tool(int id, String name, int quantity, double price, int supplierID) {
		this.id = id;
		this.name = name;
		this.quantity = quantity;
		this.price = price;
		this.supplierID = supplierID;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getID() {
		return id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * Gets the supplier ID.
	 *
	 * @return the supplier ID
	 */
	public int getSupplierID() {
		return supplierID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String tool = this.id + " " + this.name + " " + this.quantity + " " + this.price + " " + this.supplierID;
		return tool;
	}
}
