package lab8_ex3;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;

// TODO: Auto-generated Javadoc
/**
 * The Class ResultsList.
 * creates a Jlist to display the search results in
 */
public class ResultsList extends JPanel{
	
	
	/** The list of results. */
	private JList listOfResults;
	
	/** The list model. */
	private DefaultListModel listModel;
	
	
	/**
	 * Instantiates a new results list.
	 */
	public ResultsList() {
		super(new BorderLayout());
		listModel = new DefaultListModel();
		
		listOfResults = new JList(listModel);
		listOfResults.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listOfResults.setSelectedIndex(0);

        JScrollPane listScrollPane = new JScrollPane(listOfResults);
        add(listScrollPane);
	}

	/**
	 * Adds the client.
	 *
	 * @param client the client
	 */
	public void addClient(Client client) {
		listModel.addElement(client);
	}
	
	/**
	 * Clear.
	 */
	public void clear() {
		listModel.removeAllElements();
	}

	/**
	 * Gets the list of results.
	 *
	 * @return the list of results
	 */
	public JList getListOfResults() {
		return listOfResults;
	}

	/**
	 * Adds the list selection listener.
	 *
	 * @param listener the listener
	 */
	public void addListSelectionListener(ListSelectionListener listener) {
		listOfResults.addListSelectionListener(listener);
	}

	/**
	 * Gets the list model.
	 *
	 * @return the list model
	 */
	public DefaultListModel getListModel() {
		return listModel;
	}
	
}