package lab8_ex3;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

// TODO: Auto-generated Javadoc
/**
 * The Class CenterLeftPanel.
 */
public class CenterLeftPanel{
	
	/** The panel. */
	private JPanel panel;
	
	/** The top panel. */
	private CenterLeftTopPanel topPanel;
	
	/** The lower panel. */
	private CenterLeftLowerPanel lowerPanel;
	
	
	/**
	 * Instantiates a new center left panel.
	 */
	public CenterLeftPanel() {
		panel = new JPanel(new GridLayout(2, 1));
		
		topPanel = new CenterLeftTopPanel();
		lowerPanel = new CenterLeftLowerPanel();
	}

	/**
	 * Setup.
	 */
	public void setup() {
		
		
		topPanel.setup();
		panel.add(topPanel.getPanel());
		
		lowerPanel.setup();
		panel.add(lowerPanel.getPanel());
		
	}
	
	/**
	 * Gets the panel.
	 *
	 * @return the panel
	 */
	public JPanel getPanel() {
		return panel;
	}


	/**
	 * Gets the top panel.
	 *
	 * @return the top panel
	 */
	public CenterLeftTopPanel getTopPanel() {
		return topPanel;
	}

	/**
	 * Gets the lower panel.
	 *
	 * @return the lower panel
	 */
	public CenterLeftLowerPanel getLowerPanel() {
		return lowerPanel;
	}
	
	
}