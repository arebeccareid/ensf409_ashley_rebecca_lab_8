package lab8_ex3;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

// TODO: Auto-generated Javadoc
/**
 * The Class Field.
 */
public class Field {
	
	/** The label. */
	private JLabel label;
	
	/** The text. */
	private JTextField text;
	
	/** The c. */
	private GridBagConstraints c;
	
	/**
	 * Instantiates a new field.
	 *
	 * @param panel the panel
	 * @param row the row
	 * @param name the name
	 */
	public Field(JPanel panel, int row, String name) {
		c = new GridBagConstraints();
		
		
		c.weightx = 0.5;
		c.weighty = 0.5;
		
		label = new JLabel(name);
		c.gridx = 0;
		c.gridy = row;
		panel.add(label, c);
		
		text = new JTextField();
		c.gridx = 1;
		c.gridy = row;
		c.gridwidth = 2;
		text.setColumns(10);
		panel.add(text, c);
		c.gridwidth = 1;
	}
	
	/**
	 * Display.
	 *
	 * @param s the s
	 */
	public void display(String s) {
		text.setText(s);
	}
	
	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	public JLabel getLabel() {
		return label;
	}

	/**
	 * Sets the label.
	 *
	 * @param label the new label
	 */
	public void setLabel(JLabel label) {
		this.label = label;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public JTextField getText() {
		return text;
	}

	/**
	 * Sets the text.
	 *
	 * @param text the new text
	 */
	public void setText(JTextField text) {
		this.text = text;
	}
	
	
	
}