package lab8_ex3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

// TODO: Auto-generated Javadoc
/**
 * The Class ClientControl.
 */
public class ClientControl{


	/** The view. */
	private ClientView view;
	
	/** The model. */
	private RebsClientManager model;

	/**
	 * Instantiates a new client control.
	 */
	public ClientControl() {
		view = new ClientView();

		model = new RebsClientManager();
	}

	/**
	 * Run.
	 */
	public void run() {
		view.setup();

		closeFrame();
		new saveActionListener();
		new clearActionListener();
		new deleteActionListener();
		addLeftActionListeners();
		
		model.createTable();


	}


	/**
	 * Close frame.
	 */
	private void closeFrame() {

		view.getFrame().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		view.getFrame().addWindowListener( new WindowAdapter()
		{
		    public void windowClosing(WindowEvent e)
		    {
		        JFrame frame = (JFrame)e.getSource();
		 
		        int result = JOptionPane.showConfirmDialog(
		            frame,
		            "Are you sure you want to exit the application?",
		            "Exit Application",
		            JOptionPane.YES_NO_OPTION);
		 
		        if (result == JOptionPane.YES_OPTION) {
		        	model.removeTable();
		            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		        }
		    }
		});
	}
	
	
	/**
	 * Clear inputs.
	 */
	public void clearInputs() {
		view.getMainPanel().getCenter().getRight().newID();
		view.getMainPanel().getCenter().getRight().getFirstName().getText().setText("");
		view.getMainPanel().getCenter().getRight().getLastName().getText().setText("");
		view.getMainPanel().getCenter().getRight().getAddress().getText().setText("");
		view.getMainPanel().getCenter().getRight().getPostalCode().getText().setText("");
		view.getMainPanel().getCenter().getRight().getPhoneNumber().getText().setText("");
	}
	
	/**
	 * Adds the left action listeners.
	 */
	private void addLeftActionListeners() {
		
		view.getMainPanel().getCenter().getLeft().getTopPanel().addClearActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("clear search");
				view.getMainPanel().getCenter().getLeft().getLowerPanel().getResults().clear();
			}
			
		});
		
		view.getMainPanel().getCenter().getLeft().getTopPanel().addSearchActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				String searchType = view.getMainPanel().getCenter().getLeft().getTopPanel().getSearchType().getSelectedButton();
				
				String searchTerm = view.getMainPanel().getCenter().getLeft().getTopPanel().getSearchTerm().getText().getText();
				if(searchTerm == null || searchTerm.equals("")) {
					JOptionPane.showMessageDialog(null, "Please enter a search term", "Error", JOptionPane.PLAIN_MESSAGE);
					return;
				}
				
				ArrayList<Client> searchResults;
				
				if(searchType.equals("ID")) {
					int clientID = Integer.parseInt(searchTerm);
					searchResults = model.searchforID(clientID);
					
				}
				else if(searchType.equals("NAME")) {
					searchResults = model.searchforName(searchTerm);
				}
				else if(searchType.equals("TYPE")) {
					
					String shortTerm = searchTerm.substring(0, 1);
					searchResults = model.searchforType(shortTerm);
				}
				else {
					JOptionPane.showMessageDialog(null, "Error specifying search type", " Message",JOptionPane.PLAIN_MESSAGE);
					return;
				}
				
				view.getMainPanel().getCenter().getLeft().getLowerPanel().getResults().clear();
				if(searchResults.isEmpty()) {
					JOptionPane.showMessageDialog(null, "No results found", " Message",JOptionPane.PLAIN_MESSAGE);
				}
				else {
					for(Client c : searchResults)
					view.getMainPanel().getCenter().getLeft().getLowerPanel().getResults().addClient(c);
				}
				
			}
			
		});
		
		
		view.getMainPanel().getCenter().getLeft().getLowerPanel().getResults().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				int index = view.getMainPanel().getCenter().getLeft().getLowerPanel().getResults().getListOfResults().getSelectedIndex();
				if(index != -1) {
					Client selected = (Client) view.getMainPanel().getCenter().getLeft().getLowerPanel().getResults().getListModel().getElementAt(index);
//					System.out.println(selected);
					view.getMainPanel().getCenter().getRight().setAllField(selected);
				}
			}
			
		});
	}
	
	/**
	 * The listener interface for receiving saveAction events.
	 * The class that is interested in processing a saveAction
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addsaveActionListener<code> method. When
	 * the saveAction event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see saveActionEvent
	 */
	class saveActionListener{
		
		/** The id. */
		int id;
		
		/** The first name. */
		String firstName;
		
		/** The last name. */
		String lastName;
		
		/** The address. */
		String address;
		
		/** The postal code. */
		String postalCode;
		
		/** The phone number. */
		String phoneNumber;
		
		/** The client type. */
		char clientType;
		
		/**
		 * Instantiates a new save action listener.
		 */
		public saveActionListener() {
			view.getMainPanel().getCenter().getRight().getButtons().addSaveActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					
					id = Integer.parseInt(view.getMainPanel().getCenter().getRight().getClientID().getText().getText());
					firstName = view.getMainPanel().getCenter().getRight().getFirstName().getText().getText();
					lastName = view.getMainPanel().getCenter().getRight().getLastName().getText().getText();
					address = view.getMainPanel().getCenter().getRight().getAddress().getText().getText();
					postalCode = view.getMainPanel().getCenter().getRight().getPostalCode().getText().getText();
					phoneNumber = view.getMainPanel().getCenter().getRight().getPhoneNumber().getText().getText();
					clientType = view.getMainPanel().getCenter().getRight().getClientType().getSelectedButton().charAt(0);
					
					//check if id already in use, if it is, delete old field to replace with new data
					ArrayList<Client> matches = model.searchforID(id);
					if(!matches.isEmpty()) {
						model.delete(id);
					}
				
					boolean ok1 = checkInputs();
					boolean ok2 = checkPostalCode();
					boolean ok3 = checkPhoneNumber();
					if(ok1 && ok2 && ok3) {
						
						long intPhoneNumber = stripPhoneNumber();
						Client newClient = new Client(id, firstName, lastName, address, postalCode, intPhoneNumber, clientType);
						model.addItem(newClient);
						JOptionPane.showMessageDialog(null, "Saved", "Message", JOptionPane.PLAIN_MESSAGE);
					}
				}
			});
			
			
		}
		
		
		
		/**
		 * Check input lengths.
		 *
		 * @return true, if format is correct
		 */
		private boolean checkInputs() {
			//Check Inputs
			if(firstName.length() > 20) {
				JOptionPane.showMessageDialog(null, "Names must be less than 20 characters in length", "Error", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
			else if(lastName.length() > 20) {
				JOptionPane.showMessageDialog(null, "Names must be less than 20 characters in length", "Error", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
			else if(address.length() > 50) {
				JOptionPane.showMessageDialog(null, "Addresses must be less than 50 characters in length", "Error", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
			
			return true;
		}
		
		/**
		 * Check postal code.
		 *
		 * @return true, if format is correct
		 */
		private boolean checkPostalCode() {
			
			boolean result = true;
			char [] checking = postalCode.toCharArray();
			if(postalCode.length() != 7) {
				result = false;
			}
			else if(!isLetter(checking [0]) || !isLetter(checking [2]) || !isLetter(checking[5])) {
				result =  false;
			}
			else if(!isNumber(checking[1]) || !isNumber(checking[4]) || !isNumber(checking[6])) {
				result = false;
			}
			else if(checking[3] != ' ' && checking[3] != '\t')	{
				result = false;
			}
			
			if(result == false) {
				JOptionPane.showMessageDialog(null, "Make sure postal codes are in the format 'A1A 1A1' where A is a letter and 1 is a number", "Error", JOptionPane.PLAIN_MESSAGE );
			}
			return result;
		}
		
		/**
		 * Check phone number.
		 *
		 * @return true, if format is correct
		 */
		private boolean checkPhoneNumber() {
			boolean result = true;
			char [] checking = phoneNumber.toCharArray();
			
			if(phoneNumber.length() != 12) {
				result = false;
			}
			else if(!isNumber(checking[0]) || !isNumber(checking[1]) || !isNumber(checking[2]) ||!isNumber(checking[4]) ||!isNumber(checking[5]) ||!isNumber(checking[6]) ||
					!isNumber(checking[8]) ||!isNumber(checking[9]) ||!isNumber(checking[10]) ||!isNumber(checking[11])) {
				result = false;
			}
			else if(!isDash(checking[3]) || !isDash(checking[7])) {
				result = false;
			}
			
			if(result == false) {
				JOptionPane.showMessageDialog(null, "Make sure phone numbers are in the format '111-111-1111' where 1 is a number", "Error", JOptionPane.PLAIN_MESSAGE );
			}
			
			return result;
		}
		
		/**
		 * Strip phone number.
		 *
		 * @return the phone number converted into a long 
		 */
		private long stripPhoneNumber() {
			String numeric = phoneNumber.substring(0, 3) + phoneNumber.substring(4, 7) + phoneNumber.substring(8, 12);
			long integerPhoneNumber = Long.parseLong(numeric);
			return integerPhoneNumber;
		}
		
		
	}
	
	/**
	 * The listener interface for receiving clearAction events.
	 * The class that is interested in processing a clearAction
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addclearActionListener<code> method. When
	 * the clearAction event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see clearActionEvent
	 */
	class clearActionListener{
		
		/**
		 * Instantiates a new clear action listener.
		 */
		public clearActionListener(){
			view.getMainPanel().getCenter().getRight().getButtons().addClearActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {;
					clearInputs();
				}
			});
		}
	}
	
	/**
	 * The listener interface for receiving deleteAction events.
	 * The class that is interested in processing a deleteAction
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>adddeleteActionListener<code> method. When
	 * the deleteAction event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see deleteActionEvent
	 */
	class deleteActionListener{
		
		/**
		 * Instantiates a new delete action listener.
		 */
		public deleteActionListener() {
			view.getMainPanel().getCenter().getRight().getButtons().addDeleteActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					int id = Integer.parseInt(view.getMainPanel().getCenter().getRight().getClientID().getText().getText());
					model.delete(id);
				}
			});
		}
	}
	
	/**
	 * Checks if is letter.
	 *
	 * @param c the c
	 * @return true, if is a letter
	 */
	private boolean isLetter(char c) {
		if(('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z'))
			return true;
		return false;
	}
	
	/**
	 * Checks if is number.
	 *
	 * @param c the c
	 * @return true, if is a number
	 */
	private boolean isNumber(char c) {
		if('0' <= c && c <= '9')
			return true;
		return false;
	}
	
	/**
	 * Checks if is dash.
	 *
	 * @param c the c
	 * @return true, if is a dash
	 */
	private boolean isDash(char c) {
		if(c == '-')
			return true;
		return false;
	}


}