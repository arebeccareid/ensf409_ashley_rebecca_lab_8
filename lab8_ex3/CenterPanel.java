package lab8_ex3;

import java.awt.GridLayout;
import javax.swing.JPanel;

// TODO: Auto-generated Javadoc
/**
 * The Class CenterPanel.
 */
public class CenterPanel{
	
	/** The center panel. */
	private JPanel centerPanel;
	
	/** The left panel. */
	private CenterLeftPanel leftPanel;
	
	/** The right panel. */
	private CenterRightPanel rightPanel;
	
	/**
	 * Instantiates a new center panel.
	 */
	public CenterPanel() {
		centerPanel = new JPanel(new GridLayout(1, 2));
		leftPanel = new CenterLeftPanel();
		rightPanel = new CenterRightPanel();
	}
	
	/**
	 * Setup.
	 */
	public void setup() {
		
		leftPanel.setup();
		centerPanel.add(leftPanel.getPanel());
		
//		right.setup();
		centerPanel.add(rightPanel.getPanel());
	}

	/**
	 * Gets the center panel.
	 *
	 * @return the center panel
	 */
	public JPanel getCenterPanel() {
		return centerPanel;
	}

	/**
	 * Sets the center panel.
	 *
	 * @param centerPanel the new center panel
	 */
	public void setCenterPanel(JPanel centerPanel) {
		this.centerPanel = centerPanel;
	}

	/**
	 * Gets the left.
	 *
	 * @return the left
	 */
	public CenterLeftPanel getLeft() {
		return leftPanel;
	}

	/**
	 * Sets the left panel.
	 *
	 * @param left the new left panel
	 */
	public void setLeftPanel(CenterLeftPanel left) {
		this.leftPanel = left;
	}

	/**
	 * Gets the right.
	 *
	 * @return the right
	 */
	public CenterRightPanel getRight() {
		return rightPanel;
	}

	/**
	 * Sets the right panel.
	 *
	 * @param right the new right panel
	 */
	public void setRightPanel(CenterRightPanel right) {
		this.rightPanel = right;
	}
	
	
}