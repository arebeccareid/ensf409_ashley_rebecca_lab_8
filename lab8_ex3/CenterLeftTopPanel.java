package lab8_ex3;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

// TODO: Auto-generated Javadoc
/**
 * The Class CenterLeftTopPanel.
 */
public class CenterLeftTopPanel{
	
	/** The panel. */
	private JPanel panel;
	
	/** The search type. */
	private SearchTypeSelection searchType;
	
	/** The search term. */
	private Field searchTerm;
	
	/** The search. */
	private JButton search;
	
	/** The clear. */
	private JButton clear;
	
	
	
	/**
	 * Instantiates a new center left top panel.
	 */
	public CenterLeftTopPanel() {
		panel = new JPanel(new GridBagLayout());
		
		
		searchType = new SearchTypeSelection();
		
		
		
		
		search = new JButton("Search");
		
		
		clear = new JButton("Clear Search");
		
	}
	
	
	/**
	 * Setup.
	 */
	public void setup() {
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.weightx = 0.5;
		gbc.weighty = 0.5;
		int row = 0;
		
		panel.setBorder(BorderFactory.createLineBorder(Color.black));
		
		gbc.gridx = 0;
		gbc.gridy = row++; 
		panel.add(new JLabel("Search Clients"), gbc);
		
		gbc.gridy = row++;
		panel.add(searchType.getClientID(), gbc);
		
		gbc.gridy = row++;
		panel.add(searchType.getLastName(), gbc);
		
		gbc.gridy = row++;
		panel.add(searchType.getClientType(), gbc);
		
		searchTerm = new Field(panel, row++, "Search Parameter: ");
		
		gbc.gridy = row++;
		panel.add(search, gbc);
		
		gbc.gridx = 1;
		panel.add(clear, gbc);
	}
	
	
	/**
	 * Adds the clear action listener.
	 *
	 * @param al the al
	 */
	public void addClearActionListener(ActionListener al) {
		clear.addActionListener(al);
	}
	
	/**
	 * Adds the search action listener.
	 *
	 * @param al the al
	 */
	public void addSearchActionListener(ActionListener al) {
		search.addActionListener(al);
	}
	
	/**
	 * Gets the panel.
	 *
	 * @return the panel
	 */
	public JPanel getPanel() {
		return panel;
	}

	/**
	 * Gets the search type.
	 *
	 * @return the search type
	 */
	public SearchTypeSelection getSearchType() {
		return searchType;
	}

	/**
	 * Gets the search term.
	 *
	 * @return the search term
	 */
	public Field getSearchTerm() {
		return searchTerm;
	}

	/**
	 * Gets the search button.
	 *
	 * @return the search button
	 */
	public JButton getSearch() {
		return search;
	}



	/**
	 * Gets the clear button.
	 *
	 * @return the clear button
	 */
	public JButton getClear() {
		return clear;
	}



	
}