package lab8_ex3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

// TODO: Auto-generated Javadoc
/**
 * Creates a panel in the lower half of the left half of the center of the frame.
 */
public class CenterLeftLowerPanel{
	
	/** The panel. */
	private JPanel panel;
//	private JScrollPane display;
	/** The results. */
//private JTextArea textArea;
	private ResultsList results;
	
	/** The layout. */
	private BorderLayout layout;
	
	/**
	 * Instantiates a new center left lower panel.
	 */
	public CenterLeftLowerPanel() {
		panel = new JPanel(layout = new BorderLayout());
		panel.setBorder(BorderFactory.createLineBorder(Color.black));
		results = new ResultsList();
	}
	
	/**
	 * Setup the panel.
	 */
	public void setup() {
		panel.add(new JLabel("Search Results: "), layout.NORTH);
		
		panel.add(results, layout.CENTER);
		
	}
	
	
	/**
	 * Gets the panel.
	 *
	 * @return the panel
	 */
	public JPanel getPanel() {
		return panel;
	}
	
//	/**
//	 * Sets the panel.
//	 *
//	 * @param panel the new panel
//	 */
//	public void setPanel(JPanel panel) {
//		this.panel = panel;
//	}

	
	/**
	 * Gets the results.
	 *
	 * @return the results
	 */
	public ResultsList getResults() {
		return results;
	}

//	/**
//	 * Sets the results.
//	 *
//	 * @param results the new results
//	 */
//	public void setResults(ResultsList results) {
//		this.results = results;
//	}
//
//	
	
	
	
}