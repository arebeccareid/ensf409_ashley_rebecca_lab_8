package lab8_ex3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;

// TODO: Auto-generated Javadoc
/**
 * The Class MainPanel.
 */
public class MainPanel {
	
	/** The panel. */
	private JPanel panel;
	
	/** The title. */
	private JLabel title;
	
	/** The center. */
	private CenterPanel center;

	/**
	 * Instantiates a new main panel.
	 */
	public MainPanel() {
		panel = new JPanel(new BorderLayout());
		title = new JLabel("Client Management Screen");
		
		center = new CenterPanel();
	}
	
	/**
	 * Setup.
	 */
	public void setup() {
		Font labelFont = title.getFont();
		title.setFont(new Font(labelFont.getName(), Font.PLAIN, 20));
		
		panel.add(title, BorderLayout.NORTH);
		panel.setBackground(Color.orange);
		
		center.setup();
		panel.add(center.getCenterPanel(), BorderLayout.CENTER);
	}
	
	/**
	 * Gets the panel.
	 *
	 * @return the panel
	 */
	public JPanel getPanel() {
		return panel;
	}

	/**
	 * Sets the panel.
	 *
	 * @param panel the new panel
	 */
	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public JLabel getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(JLabel title) {
		this.title = title;
	}

	/**
	 * Gets the center.
	 *
	 * @return the center
	 */
	public CenterPanel getCenter() {
		return center;
	}

	/**
	 * Sets the center.
	 *
	 * @param center the new center
	 */
	public void setCenter(CenterPanel center) {
		this.center = center;
	}
	
	
	

}
