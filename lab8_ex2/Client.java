package lab8_ex2;

// TODO: Auto-generated Javadoc
/**
 * The Class Client.
 */
public class Client {

	/** The id. */
	private int id;

	/** The firstname. */
	private String firstname;

	/** The lastname. */
	private String lastname;

	/** The address. */
	private String address;

	/** The postal code. */
	private String postalCode;

	/** The phone number. */
	private int phoneNumber;

	/** The client type. */
	private char clientType;

	/**
	 * Instantiates a new client.
	 *
	 * @param id
	 *            the id
	 * @param firstname
	 *            the firstname
	 * @param lastname
	 *            the lastname
	 * @param address
	 *            the address
	 * @param postalCode
	 *            the postal code
	 * @param phoneNumber
	 *            the phone number
	 * @param clientType
	 *            the client type
	 */
	public Client(int id, String firstname, String lastname, String address, String postalCode, int phoneNumber,
			char clientType) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.address = address;
		this.postalCode = postalCode;
		this.phoneNumber = phoneNumber;
		this.clientType = clientType;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the firstname.
	 *
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * Sets the firstname.
	 *
	 * @param firstname
	 *            the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * Gets the lastname.
	 *
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * Sets the lastname.
	 *
	 * @param lastname
	 *            the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the postal code.
	 *
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * Sets the postal code.
	 *
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * Gets the phone number.
	 *
	 * @return the phoneNumber
	 */
	public int getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Sets the phone number.
	 *
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Gets the client type.
	 *
	 * @return the clientType
	 */
	public char getClientType() {
		return clientType;
	}

	/**
	 * Sets the client type.
	 *
	 * @param clientType
	 *            the clientType to set
	 */
	public void setClientType(char clientType) {
		this.clientType = clientType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// return "Client [id=" + id + ", firstname=" + firstname + ", lastname=" +
		// lastname + ", address=" + address
		// + ", postalCode=" + postalCode + ", phoneNumber=" + phoneNumber + ",
		// clientType=" + clientType + "]";

		return id + "   " + lastname + "   " + clientType;
	}

}